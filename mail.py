import smtplib
import ssl
import csv
import sys
from pyexcel.cookbook import merge_all_to_a_book
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Mail:
    def __init__(self, receiver, username, password):
        self.receiver = receiver
        self.sender = username
        self.password = password
        self.subject = "An email with attachment from Python"
        self.body = "This is an email with attachment sent from Python"

    @staticmethod
    def create_report(data):
        try:
            with open(f'{sys.path[0]}/trading_report.csv', 'w') as csv_file:
                data = data[0]
                writer = csv.DictWriter(csv_file, data.keys())
                writer.writeheader()
                writer.writerow(data)
            merge_all_to_a_book([f'{sys.path[0]}/trading_report.csv'],
                                f'{sys.path[0]}/trading_report.xlsx')
        except Exception as e:
            print("Отсутствие данных для создания отчета")

    def setup(self):
        message = MIMEMultipart()
        message['From'] = self.sender
        message['To'] = self.receiver
        message['Subject'] = self.subject
        return message

    def attach_file(self, message):
        try:
            message.attach(MIMEText(self.body, "plain"))
            filename = 'trading_report.xlsx'
            with open(filename, 'rb') as attachment:
                part = MIMEBase('application', 'octet-stream')
                part.set_payload(attachment.read())
            encoders.encode_base64(part)
            part.add_header(
                "Content-Disposition",
                f"attachment; filename= {filename}")
            message.attach(part)
            text = message.as_string()
            return text
        except Exception:
            filename = 'trading_report.xlsx'
            print(f'Файл {filename} не найден')

    def send_email(self, text):
        try:
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL('smtp.yandex.ru', 465, context=context) as server:
                server.login(self.sender, self.password)
                server.sendmail(self.sender, self.receiver, text)
            print('Отчет был создан и успешно отправлен')
        except Exception as e:
            print('Отчет не был отправлен')
