import itertools
import xlrd


class Parser:
    def __init__(self, path):
        try:
            self.content = xlrd.open_workbook(path, formatting_info=True)
            self.sheet = self.content.sheet_by_index(0)
            self.path = path
        except Exception:
            print('Путь к первичному отчету не был получен')

    @staticmethod
    def isfloat(self):
        try:
            float(self)
            return True
        except ValueError:
            return False

    def get_row(self, string: str):
        try:
            main_row = [[dirty_row.value for dirty_row in self.sheet.row(i[0]) if dirty_row.value != ''] for i in
                        itertools.product(range(self.sheet.nrows), range(self.sheet.ncols)) if
                        self.sheet.cell(i[0], i[1]).value == f'{string}']
            return main_row
        except Exception:
            pass

    def parse_client(self):
        client = self.get_row('Клиент:')
        return client[0][-1]

    def parse_period(self):
        date_period = self.get_row('Период:')
        return date_period[0][-1]

    def parse_start_period(self):
        main_row = self.get_row('Остаток денежных средств на начало периода (Рубль):')
        if not main_row:
            start_period = 0
        else:
            start_period = [i for i in main_row[0] if self.isfloat(i) and i >= 0][0]
        return start_period

    def parse_end_period(self):
        main_row = self.get_row('Остаток денежных средств на конец периода (Рубль):')
        if not main_row:
            end_period = 0
        else:
            end_period = [i for i in main_row[0] if self.isfloat(i) and i >= 0][0]
        return end_period

    def parse_exchange_fee(self):
        main_row = self.get_row('Биржевой сбор')
        if not main_row:
            exchange_fee = 0
        else:
            exchange_fee = [i for i in main_row[-1] if self.isfloat(i) and i > 0][0]
        return exchange_fee

    def parse_report_price(self):
        main_row = self.get_row('Ведение аналитического счета (Рубль)')
        if main_row:
            report_price = main_row[0][-1]
        else:
            report_price = 0
        return report_price

    def parse_company_award(self):
        main_row = self.get_row('Вознаграждение компании')
        if not main_row:
            company_reward = 0
        else:
            company_reward = [i for i in main_row[-1] if self.isfloat(i) and i > 0][0]
        return company_reward

    def parse_variation_margin(self):
        main_row = self.get_row('Вариац. маржа по операциям с фьючерсами')
        if not main_row:
            variation_margin = 0
        else:
            variation_margin = [i for i in main_row[0] if self.isfloat(i) and i > 0][0]
        return variation_margin

    def parse_taxes(self):
        main_row = self.get_row('НДФЛ')
        if main_row:
            taxes = [i for i in main_row[0] if self.isfloat(i)]
        else:
            taxes = [0]
        tax_crediting = taxes[0] if len(taxes) > 1 else taxes[0]
        tax_write_off = taxes[1] if len(taxes) > 1 else taxes[0]
        return tax_crediting, tax_write_off

    def parse_quik(self):
        main_row = self.get_row('Quik')
        if not main_row:
            quik = 0
        else:
            quik = [i for i in main_row[-1] if self.isfloat(i) and i > 0][0]
        return quik

    def parse_withdrawal(self):
        main_row = self.get_row('Вывод ДС')
        if not main_row:
            withdrawal = 0
        else:
            withdrawal = [i for i in main_row[-1] if self.isfloat(i) and i > 0][0]
        return withdrawal

    def parse_support_award(self):
        main_row = self.get_row('Вознаграждение за поддержание позиции')
        if not main_row:
            support_reward = 0
        else:
            support_reward = [i for i in main_row[-1] if self.isfloat(i) and i > 0][0]
        return support_reward

    def print_result(self):
        try:
            print(f'Клиент: {self.parse_client()}\n'
                  f'Дата: {self.parse_period()} руб.\n'
                  f'Сумма на начало дня: {self.parse_start_period()} руб.\n'
                  f'Сумма на конец дня: {self.parse_end_period()} руб.\n'
                  f'Вариационная маржа: {self.parse_variation_margin()} руб.\n'
                  f'Биржевой сбор: {self.parse_exchange_fee()} руб.\n'
                  f'Вознаграждение компании: {self.parse_company_award()} руб.\n'
                  f'Вознаграждение за поддержание позиции: {self.parse_support_award()} руб.\n'
                  f'Ведение аналитического счета: {self.parse_report_price()} руб.\n'
                  f'НДФЛ зачисление: {self.parse_taxes()[0]} руб.\n'
                  f'НДФЛ списание: {self.parse_taxes()[1]} руб.\n'
                  f'Quik: {self.parse_quik()} руб.\n'
                  f'Вывод ДС: {self.parse_withdrawal()} руб.\n')
            filtered_data = [
                {"Клиент": self.parse_client(),
                 "Дата": self.parse_period(),
                 "Сумма на начало дня": self.parse_start_period(),
                 "Сумма на конец дня": self.parse_end_period(),
                 "Вариационная маржа": self.parse_variation_margin(),
                 "Биржевой сбор": self.parse_exchange_fee(),
                 "Вознаграждение компании": self.parse_company_award(),
                 "Вознаграждение за поддержание позиции": self.parse_support_award(),
                 "Ведение аналитического счета": self.parse_report_price(),
                 "НДФЛ зачисление": self.parse_taxes()[0],
                 "НДФЛ списание": self.parse_taxes()[1],
                 "Quik": self.parse_quik(),
                 "Вывод ДС": self.parse_withdrawal()
                 }
            ]
            return filtered_data
        except Exception:
            print('Ошибка отправки данных для дальнейшей обработки')
