import os
import sys
import zipfile
import datetime as dt
from imbox import Imbox


class Report:
    def __init__(self, host=None, username=None, password=None):
        self.host = host
        self.username = username
        self.password = password

    @staticmethod
    def get_weekday():
        today = dt.date.today()
        today = dt.date(today.year, today.month, today.day)
        days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
        today_weekday = days[today.weekday()]
        # today_weekday = days[-2]
        return today_weekday

    def check_weekday(self):
        today_weekday = self.get_weekday()
        if today_weekday == 'sunday' or today_weekday == 'monday':
            return True
        else:
            return False

    @staticmethod
    def create_path_if_not_exists():
        directory = os.path.dirname(sys.path[0] + '/attachments/')
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory

    @staticmethod
    def get_path_to_file():
        path_to_file = ''
        for dirpath, dirnames, files in os.walk(Report.create_path_if_not_exists()):
            for filename in files:
                check_filename = filename.split(' ')
                if check_filename[1] == "B" and check_filename[-1] == 'N_1.xls':
                    path_to_file = os.path.join(dirpath, filename)
        return path_to_file

    def get_content(self):
        try:
            mail = Imbox(self.host, self.username, self.password, ssl=True, ssl_context=None, starttls=False)
            messages = mail.messages(sent_from='noreply@bcs.ru', date__on=dt.date((dt.date.today()).year,
                                                                                  (dt.date.today()).month,
                                                                                  (dt.date.today()).day))
            for (uid, message) in messages:
                for idx, attachment in enumerate(message.attachments):
                    filename = attachment.get('filename')
                    Report.save_content(filename, attachment)
        except Exception:
            print('Ошибка подключения к сети')

    @staticmethod
    def save_content(filename, attachment):
        if (filename.split('.')[-1]) == 'zip':
            with open(f'attachments/{filename}', 'wb') as content:
                content.write(attachment.get('content').read())
            with zipfile.ZipFile(f'attachments/{filename}', 'r') as unzip:
                unzip.extractall('attachments')
        else:
            with open(f'attachments/{filename}', 'wb') as content:
                content.write(attachment.get('content').read())

    @staticmethod
    def remove_file(path):
        # removing the file
        if not os.remove(path):
            pass
        else:
            # failure message
            print(f"Unable to delete the {path}")

    def delete_files(self):
        for dirpath, dirnames, files in os.walk(self.create_path_if_not_exists()):
            for file in files:
                file_path = os.path.join(dirpath, file)
                self.remove_file(file_path)
        for dirpath, dirnames, files in os.walk(sys.path[0]):
            for file in files:
                if file == 'trading_report.csv':
                    file_path = os.path.join(dirpath, file)
                    self.remove_file(file_path)
                elif file == 'trading_report.xlsx':
                    file_path = os.path.join(dirpath, file)
                    self.remove_file(file_path)
