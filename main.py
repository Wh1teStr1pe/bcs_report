def main():
    start = time.time()
    bcs_report = Report(host="host",
                        username="username",
                        password='password')
    if bcs_report.check_weekday() is True:
        print('Сегодня отчетности не ожидается')
    else:
        bcs_report.create_path_if_not_exists()
        bcs_report.get_content()
        path = bcs_report.get_path_to_file()
        data = Parser.print_result(Parser(path))
        Mail.create_report(data)
        login = Mail('some_email@yandex.ru', (bcs_report.username + '@some_email.ru'), bcs_report.password)
        message = Mail.setup(login)
        content = Mail.attach_file(login, message)
        Mail.send_email(login, content)
        bcs_report.delete_files()
    end = time.time()
    if bcs_report.check_weekday() is False:
        print(f'\nFinished in {format_timespan(end - start)}')


if __name__ == '__main__':
    from mail import Mail
    from parser import Parser
    from report import Report
    from humanfriendly import format_timespan
    import time

    main()